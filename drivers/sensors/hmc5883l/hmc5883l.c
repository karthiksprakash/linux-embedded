#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/types.h>

#define HMC5883L_I2C_ADDRESS    0x1E
#define HMC5883L_DATA_OUT_X_MSB_REG 0X03
#define HMC5883L_DATA_OUT_X_LSB_REG 0X04
#define HMC5883L_DATA_OUT_Y_MSB_REG 0X07
#define HMC5883L_DATA_OUT_Y_LSB_REG 0X08
#define HMC5883L_DATA_OUT_Z_MSB_REG 0X05
#define HMC5883L_DATA_OUT_Z_LSB_REG 0X06

#define HMC5883L_DRIVER_NAME "hmc5883l"

static const unsigned short normal_i2c[] = {HMC5883L_I2C_ADDRESS, I2C_CLIENT_END};

/*Device interface functions*/
static s32 hmc5883l_read_measurements(struct i2c_client *client, s16 *data)
{
    /* Read all data output registers*/
    return i2c_smbus_read_i2c_block_data(client, HMC5883L_DATA_OUT_X_MSB_REG, 0x06, (u8 *) data);
}

/*sysfs callback functions*/
static ssize_t show_values(struct device *dev, char *buf)
{
    s16 data[3];
    struct i2c_client *client = to_i2c_client(dev);

    s32 result = hmc5883l_read_measurements(client, data);

    if(result > 0){
        return sprintf(buf, "%d,%d,%d\n",
                (s16)swab16((u16)data[0]),
                (s16)swab16((u16)data[1]),
                (s16)swab16((u16)data[2]));
    }
    return result;
}
static DEVICE_ATTR(values, S_IRUGO, show_values, NULL);


static hmc5883l_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    printk(KERN_INFO "%s: new instance found!\n", HMC5883L_DRIVER_NAME);
}

static hmc5883l_remove(struct i2c_client *client)
{
    return 0;
}

static const struct i2c_device_id hmc5883l_id[] = {
    { HMC5883L_DRIVER_NAME, 0},
    {}
};

static struct i2c_driver hmc5883l_driver ={
    .driver.name = HMC5883L_DRIVER_NAME,
    .id_table = hmc5883l_id,
    .probe = hmc5883l_probe,
    .remove = hmc5883l_remove,
};

static __init hmc5883l_init(void)
{
    printk(KERN_INFO "%s: init!", HMC5883L_DRIVER_NAME );
}

static __exit hmc5883l_exit(void)
{
    printk(KERN_INFO "%s: exit!", HMC5883L_DRIVER_NAME );
}

MODULE_AUTHOR("Karthik Sudandraprakash <karvi_in@yahoo.com");
MODULE_DESCRIPTION("HMC5883L driver");
MODULE_LICENSE("GPL");

module_init(hmc5883l_init);
module_exit(hmc5883l_exit);

