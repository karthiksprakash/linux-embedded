/* 
 * File:   I2CLinuxDev.cpp
 * Author: ksudandr
 *
 * Created on July 22, 2013, 10:43 AM
 */

#include "I2CLinuxDev.h"

/** Set measurement bias value.
* Initializes the device for read/write operation
*/ 
bool I2CLinuxDev::init()
 {    
    return (openI2CBus() && selectDevice());    
 }    

/** Set measurement bias value.
* Performs an ioctl call to set the device as a slave.
*/
bool I2CLinuxDev::selectDevice() {
    bool returnFlag = false;
    if(m_isI2CBusOpen)
    {
        if (ioctl(m_i2cBusFd, I2C_SLAVE, devAddr) < 0) 
        {
            fprintf(stderr, "Failed to acquire bus access and/or talk to slave: %s\n", strerror(errno)); 
        }
        else
        {
            returnFlag = true;
        }
    }
    return returnFlag;
}

/** Set measurement bias value.
* Opens the I2C bus represented by the Linux file system
*/
bool I2CLinuxDev::openI2CBus() { 
    if(!m_isI2CBusOpen)
    {
        if ((m_i2cBusFd = open(m_i2cBusFileName.c_str(), O_RDWR)) < 0) 
        {
            fprintf(stderr, "Failed to open the i2c bus: %s. %s\n", m_i2cBusFileName.c_str(), strerror(errno));              
        }
        else
        {        
            m_isI2CBusOpen = true;
        }
    }
    return m_isI2CBusOpen;
}


/** Set measurement bias value.
* @param reg The register address to write into
* @param val The 8-bit value to write 
*/
void I2CLinuxDev::writeByte(const u_int8_t reg, const u_int8_t val) {
    u_int8_t buf[2];
    buf[0] = reg; 
    buf[1] = val;
    
   if (write(m_i2cBusFd, buf, 2) != 2) {
    fprintf(stderr, "Failed to write to the i2c bus: %s\n", strerror(errno));  
   }        
}

/** Set measurement bias value.
* @param reg The register address to read from
* @param val The 8-bit value to write 
*/
u_int8_t I2CLinuxDev::readByte(u_int8_t reg, u_int8_t *data) 
{
    return readBytes(reg, data, 1);
}

/** Set measurement bias value.
* @param reg The address of the starting register to read from
* Note: It is assumed that the I2C device will auto increment the register address
* @param buf The byte array to store the read values
* @param count The number of bytes to expect
*/
u_int8_t I2CLinuxDev::readBytes(u_int8_t reg, u_int8_t *buf, int count) 
{
    u_int8_t writebuf[1];
    writebuf[0] = reg;
    int readByteCount = 0;
    
    if (write(m_i2cBusFd, writebuf, 1) != 1) {
        fprintf(stderr, "Failed to write to the i2c bus to select the register: %s\n", strerror(errno));  
        goto Exit;
    }
    
    readByteCount = read(m_i2cBusFd, buf, count);
    if (readByteCount != count) {
        fprintf(stderr, "Failed to write to the i2c bus: %s\n", strerror(errno));  
    }
   
Exit:
    return readByteCount;
}

/** Set measurement bias value.
* Closes the I2C Bus file if it was opened
*/
void I2CLinuxDev::closeDevice()
{
    if(m_isI2CBusOpen)
    {
        //Close the I2C Bus file
        close(m_i2cBusFd);
        m_isI2CBusOpen = false;
    }
}

/** Set measurement bias value.
* Destructor. Closes the I2C Bus file
*/
I2CLinuxDev::~I2CLinuxDev()
{
    this->closeDevice();
}