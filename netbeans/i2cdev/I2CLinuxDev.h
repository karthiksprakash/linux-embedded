/* 
 * File:   I2CLinuxDev.h
 * Author: ksudandr
 *
 * Created on July 22, 2013, 10:43 AM
 */

#ifndef LIBI2CLINUXDEV_H
#define	LIBI2CLINUXDEV_H

#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/types.h>
#include <linux/i2c-dev.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include "I2CDev.h"


class I2CLinuxDev : public I2CDev {
    
    private:
        std::string m_i2cBusFileName;
        int m_i2cBusFd;        
        bool m_isI2CBusOpen;        
        
        I2CLinuxDev(){};
        
    public:        
        I2CLinuxDev(const std::string& i2cBusFileName, u_int8_t address): I2CDev(address), m_i2cBusFileName(i2cBusFileName){}
        
        bool selectDevice();
        bool openI2CBus();
        
        virtual bool init();
        virtual void writeByte(const u_int8_t reg, const u_int8_t val);
        virtual u_int8_t readByte(const u_int8_t reg, u_int8_t *data);
        virtual u_int8_t readBytes(const u_int8_t reg, u_int8_t *buf, int count);
        virtual void closeDevice();
        ~I2CLinuxDev();
};

#endif	/* LIBI2CLINUXDEV_H */

