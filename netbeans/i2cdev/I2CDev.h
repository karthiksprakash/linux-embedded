/* 
 * File:   I2CDev.h
 * Author: ksudandr
 *
 * Created on July 22, 2013, 10:43 AM
 */

#ifndef LIBI2CDEV_H
#define	LIBI2CDEV_H

#include <sys/types.h>


class I2CDev {
    private:
        
        
    protected:
        u_int8_t devAddr;                
        I2CDev(){};        
        
    public:        
        I2CDev(u_int8_t address) {devAddr = address;}
        virtual ~I2CDev() {};
        
        /** Set measurement bias value.
        * Initializes the device for read/write operation
        */ 
        virtual bool init() = 0;
        
        /** Set measurement bias value.
        * @param reg The register address to write into
        * @param val The 8-bit value to write 
        */
        virtual void writeByte(const u_int8_t reg, const u_int8_t val) = 0;
        
        /** Set measurement bias value.
        * @param reg The register address to read from
        * @param val The 8-bit value to write 
        */
        virtual u_int8_t readByte(const u_int8_t reg, u_int8_t *data) = 0;
        
        /** Set measurement bias value.
        * @param reg The address of the starting register to read from        
        * @param buf The byte array to store the read values
        * @param count The number of bytes to expect
        */
        virtual u_int8_t readBytes(const u_int8_t reg, u_int8_t *buf, int count) = 0;
        
        /** Set measurement bias value.
        * Closes the I2C Bus file if it was opened
        */
        virtual void closeDevice() = 0;
};

#endif	/* LIBI2CDEV_H */

